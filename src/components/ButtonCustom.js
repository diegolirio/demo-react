import React, { Component } from 'react';

export default class ButtonCustom extends Component {

    render() {
        return(
            <button type={this.props.type} className={'pure-button '+this.props.buttonCss}>{this.props.label}</button>
        );
    }

}