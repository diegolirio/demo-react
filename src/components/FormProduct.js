import React, {Component} from 'react';
import InputCustom from './InputCustom';
import ButtonCustom from './ButtonCustom';
import $ from 'jquery';
import Pubsub from 'pubsub-js';

export default class FormProduct extends Component {

    constructor() {
        super();
        this.URL = "http://localhost:3001/products";
        //this.enviaForm = this.enviaForm.bind(this);
        this.state = {product: {id: null, code: "1sfd", description: "", valueUnit: 0}};
        this.setCode = this.setCode.bind(this);
        this.setDescription = this.setDescription.bind(this);
        this.setValueUnit = this.setValueUnit.bind(this);
      }

    setCode(ev) {
        this.state.product.code = ev.target.value;
        this.setState(this.state);
    }

    setDescription(ev) {
        this.state.product.description = ev.target.value;
        this.setState(this.state);
    }

    setValueUnit(ev) {
        this.state.product.valueUnit = ev.target.value;
        this.setState(this.state);
    }      

    render() {
        return(
            <form className="pure-form pure-form-stacked" onSubmit={this.enviaForm.bind(this)} method="post">
                <fieldset>
                    <legend>Cadastro</legend>

                    <div className="pure-g">
                        <InputCustom label="Code" id="code" name="code" type="text" value={this.state.product.code} change={this.setCode}></InputCustom> 
                        <InputCustom label="Description" id="description" name="description" type="text" value={this.state.product.description} change={this.setDescription}></InputCustom> 
                        <InputCustom label="Price" id="valueUnit" name="valueUnit" type="text" value={this.state.product.valueUnit} change={this.setValueUnit}></InputCustom> 
                    </div>

                    <ButtonCustom type="submit" buttonCss="pure-button-primary" label="Save"></ButtonCustom>
                </fieldset>
            </form>
        );
    }

    getId = function() {
        var today = new Date();
        return today.getFullYear()+""+today.getMonth()+""+today.getDate()+""+today.getHours()+""+today.getSeconds()+""+today.getMilliseconds();
    }

    enviaForm(ev) {
        ev.preventDefault();    
        console.log('Enviando...');
        var _id = this.getId();
        //var json = {id: Number.parseInt(_id), code: "asasa", description: "Chave", valueUnit: 300};
        this.state.product.id = Number.parseInt(_id);
  
        $.ajax({
          url: "http://localhost:3001/products",
          dataType: 'json',
          contentType: 'application/json',
          type: 'post',
          data: JSON.stringify(this.state.product),
          success: (resposta => {
              //console.log(resposta);
              //this.state.products.push(resposta);
              //this.setState(this.state);
              //this.props.callBackListProduct(resposta);
              Pubsub.publish('new-product', resposta);
          }),
          error: error => {
              console.log(error);
          }
        });       
    }    

}