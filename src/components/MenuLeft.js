import React, {Component} from 'react';

export default class MenuLeft extends Component {

    render() {
        return(
            <div id="menu">
                <div className="pure-menu">
                    <a className="pure-menu-heading" href="#">GetTruck</a>
        
                    <ul className="pure-menu-list">
                        <li className="pure-menu-item"><a href="#" className="pure-menu-link">Home</a></li>
                        <li className="pure-menu-item"><a href="#" className="pure-menu-link">Pedido</a></li>
        
                        <li className="pure-menu-item menu-item-divided pure-menu-selected">
                            <a href="#" className="pure-menu-link">Produto</a>
                        </li>
        
                        <li className="pure-menu-item"><a href="#" className="pure-menu-link">Cliente</a></li>
                    </ul>
                </div>
            </div>
        );
    }

}