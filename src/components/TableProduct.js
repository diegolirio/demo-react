import React, {Component} from 'react';

export default class TableProduct extends Component {

    render() {
        return(
            <table className="pure-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Code</th>
                        <th>Description</th>
                        <th>Price</th>
                    </tr>
                </thead>

                <tbody>
                    {
                    this.props.products.map(function(product) {
                        return (
                        <tr key={product.id}>
                            <td>{product.id}</td>
                            <td>{product.code}</td>
                            <td>{product.description}</td>
                            <td>{product.valueUnit}</td>
                        </tr>
                        );
                    })
                    }
                </tbody>
            </table>        
        );
    }

}