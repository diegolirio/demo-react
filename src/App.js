import React, { Component } from 'react';
import './css/side-menu.css';
import './css/pure-min.css';
import $ from 'jquery';
import TableProduct from './components/TableProduct';
import MenuLeft from './components/MenuLeft';
import FormProduct from './components/FormProduct';
import Pubsub from 'pubsub-js';

class App extends Component {

  constructor() {
    super();
    this.state = {products: [], product: {id: null, code: "1sfd", description: "", valueUnit: 0}};
  }

  componentDidMount() {
    $.ajax({
        url: "http://localhost:3001/products",
        dataType: 'json',
        success: (resposta => {
            this.setState({products:resposta});
        }).bind(this)
      }); 
      Pubsub.subscribe('new-product', function(topic, newProduct) {
          this.state.products.push(newProduct);
          this.setState(this.state);        
      }.bind(this));
  }

  render() {
    return (

        <div id="layout">
            <a href="#menu" id="menuLink" className="menu-link">
                <span></span>
            </a>
            <MenuLeft></MenuLeft>
            <div id="main">
                <div className="header">
                    <h1>Produto</h1>
                </div>
                <div className="content">
                    <FormProduct callBackListProduct={this.updateListProduct}></FormProduct>
                    <TableProduct products={this.state.products}></TableProduct>
                </div>
            </div>
        </div>
    );
  }

  updateListProduct(product) {    
      //console.log(this.state.products);
    //this.state.products.push(product);
    //this.setState(this.state);
    //this.componentDidMount();
    console.log(product);
  }

  loadProducts() {
      
  }

}

export default App;
